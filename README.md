## Better Weekdays Pattern Lab

Based on [Centaurus](https://wrapbootstrap.com/theme/centaurus-WB0CX3745)

## Installation

1. Clone the repo
1. Run `npm install`
1. Create a folder called `public`
1. Copy `core/styleguide` into `public`, so you have `public/styleguide`
1. Run `grunt`

## Watching and Building

1. Run `grunt watchPL`
1. A new window with a server listening should open

## Compiling SASS

1. Set your own sass compiler to watch `source/sass`

## Known Issues
1. If you're on a Mac, using Codekit to transpile Sass, you may need to modify a Mustache file first, because PatternLab will recognize a Sass change.