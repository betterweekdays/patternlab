module.exports = function(grunt) {
    require('time-grunt')(grunt); // shows how long grunt tasks take ~ https://github.com/sindresorhus/time-grunt
    require('load-grunt-tasks')(grunt);

    grunt.initConfig({
        // https://github.com/gruntjs/grunt-shell
        watch: {
            // Watch our Sass files for changes...
            sass: {
                files: [ 'source/css/**/*.{scss,sass}' ],
                tasks: [ 'compass' ],
                options: {
                    spawn: false
                }
            },
            // Keep an eye on the Pattern Lab pattern templates...
            html: {
                files: [
                    'source/_patterns/**/*.mustache',
                    'source/_patterns/**/*.json',
                    'source/_data/*.json'
                ],
                tasks: [ 'shell:patternlab_watch' ],
                options: {
                    spawn: false,
                    livereload: true
                }
            }
        },

        // https://github.com/gruntjs/grunt-contrib-compass
        compass: {
            dist: {
                options: {
                    sassDir: 'source/css/',
                    cssDir: 'public/css/'
                }
            }
        },
        // https://github.com/gruntjs/grunt-shell
        shell: {
            bower_install: {
                command: "bower install"
            },
            patternlab_build: {
                command: "php core/builder.php -g"
            },
            patternlab_watch: {
                command: "php core/builder.php --watch --autoreload --nocache"
            }
        },

        // https://github.com/gruntjs/grunt-wiredep
        wiredep: {
            patternlab: {
                src: [
                    'source/_patterns/00-atoms/00-meta/**/*.mustache'
                ],
                ignorePath: '../'
            }
        },

        // https://www.npmjs.org/package/grunt-contrib-connect
        connect: {
            server: {
                options: {
                    port: 9001,
                    useAvailablePort: true,
                    base: 'public',
                    livereload: true,
                    open: true
                }
            }
        },

        // https://github.com/dylang/grunt-notify
        notify: {
            build: {
                options: {
                    message: 'Build Complete'
                }
            },
            done: {
                options: {
                    message: 'Done!'
                }
            }
        }
    });

    var tasksDefault = [
        'shell:patternlab_build'
    ];

    var tasksPLWatch = [
        'default',
        'connect',
        'watch'
    ];

    // Register PL tasks
    grunt.registerTask('default', tasksDefault);
    grunt.registerTask('watchPL', tasksPLWatch);
};